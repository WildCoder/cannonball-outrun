#!/bin/sh

delayed_reload(){
  # This is a super weird work around to allow for 
  # OutRun to reinitialise the weston display.
  # Don't ask me how, it just works.
  sleep 2
  ./cannonball > "/dev/null" 2>&1 &
  PID=$!
  sleep 1
  kill ${PID}
}

kill_it_with_fire(){
  n=0
  while [ "${n}" -lt 50 ]; do
    n=$(( n + 1 ))
    killall cannonball &> "/dev/null"
    sleep 0.1
  done
}

source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "cannonball"
/media/project_eris/bin/sdl_display "/var/volatile/launchtmp/warning.png" &
PID=$!
sleep 2
kill ${PID}
#HOME="/var/volatile/launchtmp" 
delayed_reload &
echo -n 2 > "/data/power/disable"
LD_PRELOAD="${PROJECT_ERIS_PATH}/lib/sdl_remap_arm.so" ./cannonball &> "${RUNTIME_LOG_PATH}/outrun.log"
echo -n 1 > "/data/power/disable"
echo "launch_StockUI" > "/tmp/launchfilecommand"
kill_it_with_fire &